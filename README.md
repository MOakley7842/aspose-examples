# README #

Includes several Aspose to examples for converting MS Office formats to PDF and Tiff to PDF.  Also include examples using PDFWriter to convert Tiff to PDF and itextpdf to convert Tiff to PDF.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Aspose Components
The Aspose for Java Components required are
-	Aspose.Cells for Java used for converting Excel spreadsheets to PDF and thumbnails
-	Aspose.Words for Java used for converting DOC, OOXML, RTF, HTML and OpenDocument formats to PDF and thumbnails
-	Aspose.Pdf for Java
-	Aspose.Slides for Java used for converting PPT, PPS, POT, PresentationML (OOXML, PPTX) and Open Document Presentations (ODP) to PDF and thumbnails.
On line pricing shows that once your requirements include 3 or more components, then the price advantage over the purchase of Aspose.Total for Java is lost.

Aspose License File
Aspose licensing is provided in a license file.  This license file must be active once per application or process.  The license file is defined as a resource in pom.xml and loading that resource as an input stream prior to setting the licenses.  Still using an evaluation license for Aspose.Total for Java.
            <resource>
                <directory>src/main/resources/licenses</directory>
                <targetPath>licenses</targetPath>
                <includes>
                    <include>Aspose.Total.Java.lic</include>
                </includes>
            </resource>

The license file name can be changed.

Aspose for Maven
In 2014 Aspose added a repository to facilitate support of maven projects.  The following repository was added to the pom.xml file.
        <repository>
            <id>AsposeJavaAPI</id>
            <name>Aspose Java API</name>
            <url>http://maven.aspose.com/artifactory/simple/ext-release-local/</url>
        </repository>

The following dependencies have been added as well.  Please note that minor version are never available from the repository and since first set up 2 of these components have been updated.  The versions listed below are as of February 23, 2015.
        <dependency>
            <groupId>com.aspose</groupId>
            <artifactId>aspose-pdf</artifactId>
            <version>9.7.1</version>
            <classifier>jdk16</classifier>
        </dependency>
        <dependency>
            <groupId>com.aspose</groupId>
            <artifactId>aspose-words</artifactId>
            <version>15.1.0</version>
            <classifier>jdk16</classifier>
        </dependency>
        <dependency>
            <groupId>com.aspose</groupId>
            <artifactId>aspose-cells</artifactId>
            <version>8.3.2</version>
        </dependency>
        <dependency>
            <groupId>com.aspose</groupId>
            <artifactId>aspose-slides</artifactId>
            <version>15.1.0</version>
            <classifier>jdk17</classifier>
        </dependency>

Aspose.Words for Java
The first step was to install the Microsoft True Type Fonts.
sudo apt-get install ttf-mscorefonts-installer

The sample document Template.doc included wingdings and symbols.  These are not included in the Microsoft True Type Fonts package so I copied the ttf files from windows to: 
/usr/share/fonts/truetype/msttcorefonts

Even the MicroSoft True Type fonts was incomplete.  I wound up copying more ttf files from my windows environment.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact