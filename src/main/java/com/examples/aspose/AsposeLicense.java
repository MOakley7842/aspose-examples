package com.examples.aspose;

import java.io.InputStream;

/**
 * Created by moakley on 3/24/15.
 */
public class AsposeLicense {
    private static final String ASPOSE_FORMAT_STRING = "licenses/Aspose.%s.Java.lic";

    public static final String ASPOSE_TOTAL = "Total";
    public static final String ASPOSE_CELLS = "Cells";
    public static final String ASPOSE_PDF = "Pdf";
    public static final String ASPOSE_SLIDES = "Slides";
    public static final String ASPOSE_WORDS = "Words";

    public static void setLicenseByType(String licByType) throws Exception {
        //InputStream stream = null;
        try
        {
            String asposeLicenseFile = pathForLicensFile(ASPOSE_TOTAL);
            InputStream stream=AsposeLicense.class.getClassLoader().getResourceAsStream(asposeLicenseFile);

            if (stream == null)
                throw new Exception(String.format("Error Aspose license file %s not found", asposeLicenseFile));


            switch (licByType) {
                case ASPOSE_CELLS:
                    com.aspose.cells.License cellsLicense=new com.aspose.cells.License();
                    cellsLicense.setLicense(stream);
                    break;
                case ASPOSE_PDF:
                    com.aspose.pdf.License pdfLicense=new com.aspose.pdf.License();
                    pdfLicense.setLicense(stream);
                    break;
                case ASPOSE_SLIDES:
                    com.aspose.slides.License slidesLicense = new com.aspose.slides.License();
                    slidesLicense.setLicense(stream);
                    break;
                case ASPOSE_WORDS:
                    com.aspose.words.License wordsLicense=new com.aspose.words.License();
                    wordsLicense.setLicense(stream);
                    break;
                case ASPOSE_TOTAL:
                    com.aspose.cells.License cellsByTotalLicense=new com.aspose.cells.License();
                    cellsByTotalLicense.setLicense(stream);

                    com.aspose.pdf.License pdfByTotalLicense=new com.aspose.pdf.License();
                    pdfByTotalLicense.setLicense(stream);

                    com.aspose.slides.License slidesByTotalLicense = new com.aspose.slides.License();
                    slidesByTotalLicense.setLicense(stream);

                    com.aspose.words.License wordsByTotalLicense=new com.aspose.words.License();
                    wordsByTotalLicense.setLicense(stream);
                    break;
                default:
                    throw new Exception(String.format("License type %s not supported", licByType));
            }

        } catch (Exception ex) {
            throw new Exception("Error setting Aspose license:", ex);
        }

    }

    private static String pathForLicensFile(String licByType) {

        return String.format(ASPOSE_FORMAT_STRING, licByType);
    }
}
