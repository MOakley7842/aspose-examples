package com.examples.aspose;

import java.awt.Color;
import java.io.*;

//import com.lowagie.text.*;
//import com.lowagie.text.pdf.*;
import com.itextpdf.text.*;
import com.itextpdf.text.io.FileChannelRandomAccessSource;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.pdf.*;
import java.awt.image.RenderedImage;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.nio.channels.FileChannel;
import java.util.Hashtable;
import javax.media.jai.NullOpImage;
import javax.media.jai.OpImage;

import com.itextpdf.text.pdf.codec.TiffImage;
import com.sun.media.jai.codec.*;
import com.sun.media.jai.codecimpl.*;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.TIFFDecodeParam;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.ImageCodec;

/**
 * Created by moakley on 1/26/15.
 */
public class PdfWriterTiffToPdf {

    private void convertTiff1(String filename, String filesave) {
        // creation of the document with a certain size and certain margins
        //Document document = new Document(PageSize.A4, 0, 0, 0, 0);
        //Document document = new Document(PageSize.A4.rotate());
        Document document = new Document();
        //Document.compress = false;
        try {
            // creation of the different writers
            FileOutputStream fos = new FileOutputStream(filesave);

            //testCaseFileInputStream = getSourceFile();
            //FileOutputStream rech=new FileOutputStream("c:\\temp.pdf");
            PdfWriter writer = PdfWriter.getInstance(document, fos);

            System.out.println(filename);
            File file = new File(filename);
            SeekableStream stream = new FileSeekableStream(filename);
            TIFFDirectory dir = new TIFFDirectory(stream, 0);
            String[] names = ImageCodec.getDecoderNames(stream);
            ImageDecoder dec =
                    ImageCodec.createImageDecoder(names[0], stream, null);

            // Which of the multiple images in the TIFF file do we want to load
            // 0 refers to the first, 1 to the second and so on.
            int total = dec.getNumPages();
            document.open();
            PdfContentByte cb = writer.getDirectContent();
            for (int k = 0; k < total; ++k) {
                RenderedImage ri = dec.decodeAsRenderedImage(k);
                Raster ra = ri.getData();
                BufferedImage bi = new BufferedImage(ri.getColorModel(),
                        Raster.createWritableRaster(ri.getSampleModel(), ra.getDataBuffer(), null), false, new Hashtable());
                Image img = Image.getInstance(bi, null, true);

                long h = 0;
                long w = 0;
                long IFDOffset = dir.getIFDOffset();
                while (IFDOffset != 0L) {
                    dir = new TIFFDirectory(stream, IFDOffset, 0);
                    IFDOffset = dir.getNextIFDOffset();
                    h = dir.getFieldAsLong(TIFFImageDecoder.TIFF_IMAGE_LENGTH);
                    w = dir.getFieldAsLong(TIFFImageDecoder.TIFF_IMAGE_WIDTH);
                }
                float percent = 100;
                int pos = 0;
                if (w > 895)
                    percent = ((595 + 18) * 100 / w);
                if (h > 842)
                    pos = (int) (842 - h * percent / 100);
                else
                    pos = (int) (842 - h);
                System.out.println(percent);
                System.out.println(pos);
                img.scalePercent(percent);
                img.setAbsolutePosition(0, pos);
                System.out.println("Image: " + k);

                cb.addImage(img);
                document.newPage();
            }
            document.close();
        } catch (Exception de) {
            de.printStackTrace();
        }
    }


    private void convertTiff2(String filename, String filesave)
    {
        try {
//Read the Tiff File
            RandomAccessFileOrArray tiffFile = new RandomAccessFileOrArray(new RandomAccessSourceFactory().createBestSource(filename));

//Find number of images in Tiff file
            int numberOfPages = TiffImage.getNumberOfPages(tiffFile);
            Image tempImage = TiffImage.getTiffImage(tiffFile, 1);
            System.out.println(String.format("filename -- %s, - %d", filename, numberOfPages));
//must set the size of the pdf document in the constructor
//all images must be the same size
            Document pdfDocument = new Document(new Rectangle(tempImage.getWidth(), tempImage.getHeight()));
            pdfDocument.open();

            PdfWriter writer = PdfWriter.getInstance(pdfDocument, new FileOutputStream(filesave));
            writer.setStrictImageSequence(true);

//Run a for loop to extract images from Tiff file
//into a Image object and add to PDF recursively
            for (int i = 2; i <= numberOfPages; i++) {
                tempImage = TiffImage.getTiffImage(tiffFile, 1);
                System.out.println(String.format("tif -- %s, %s, %d", tempImage.getHeight(), tempImage.getWidth(), i));
                //System.out.println(String.format("pdf -- %s,%s", tifftoPDFDocument.getPageSize().getHeight(), tifftoPDFDocument.getPageSize().getWidth()));
                pdfDocument.add(tempImage);
                pdfDocument.newPage();
            }
        } catch (Exception de) {
            de.printStackTrace();
        }
    }

    private void convertTiff3(String filename, String filesave) {
        try{
            //Read the Tiff File
            RandomAccessFileOrArray myTiffFile = new RandomAccessFileOrArray(filename);

            //Find number of images in Tiff file
            int numberOfPages = TiffImage.getNumberOfPages(myTiffFile);
            System.out.println(String.format("filename -- %s, - %d", filename, numberOfPages));

            //Document tifftoPDF = new Document();
            Image tempImage = TiffImage.getTiffImage(myTiffFile, 1);
            Document tifftoPDF = new Document(new Rectangle(tempImage.getWidth(), tempImage.getHeight()));
            PdfWriter.getInstance(tifftoPDF, new FileOutputStream(filesave));
            tifftoPDF.open();

            //Run a for loop to extract images from Tiff file
            //into a Image object and add to PDF recursively
            for(int i = 1;i <= numberOfPages;i++){
                tempImage = TiffImage.getTiffImage(myTiffFile, i);
                System.out.println(String.format("tif -- %s, %s, %d", tempImage.getHeight(), tempImage.getWidth(), i));
                tifftoPDF.add(tempImage);
            }

            tifftoPDF.close();
            System.out.println("Tiff to PDF Conversion in Java Completed" );
        }
        catch (Exception i1){
            i1.printStackTrace();
        }

    }

    private void convertTiff4(String filename, String filesave) {
        try {
            RandomAccessFile aFile = new RandomAccessFile(filename, "r");
            FileChannel inChannel = aFile.getChannel();
            FileChannelRandomAccessSource fcra =  new FileChannelRandomAccessSource(inChannel);
            Document pdfDocument = new Document();
            PdfWriter.getInstance(pdfDocument,  new FileOutputStream(filesave));
            pdfDocument.open();
            RandomAccessFileOrArray rafa = new RandomAccessFileOrArray(fcra);
            int pages = TiffImage.getNumberOfPages(rafa);
            System.out.println(String.format("filename -- %s, - %d", filename, pages));
            Image image;
            float width;
            float height;
            for (int i = 1; i <= pages; i++) {
                image = TiffImage.getTiffImage(rafa, i);
                width = image.getHeight();
                height = image.getWidth();
                System.out.println(String.format("tif -- %s, %s, %d", height, width, i));
                Rectangle pageSize = new Rectangle(height, width);
                // without this the page contained just the upper left part of the image
                pdfDocument.setPageSize(pageSize);
                // setting margins to 0 prevents images from being added slightly low and to the right.
                pdfDocument.setMargins(0f, 0f, 0f, 0f);
                pdfDocument.newPage();
                pdfDocument.add(image);
            }
            pdfDocument.close();
            aFile.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {
        try {
            PdfWriterTiffToPdf pdfWriter = new PdfWriterTiffToPdf();

            String url = PptToPdfConvert.class.getResource("/test/image/").getPath();

            //pdfWriter.convertTiff1(url + "3pagetiff.tif", url + "3pagetiff1.pdf");
            //pdfWriter.convertTiff2(url + "3pagetiff.tif", url + "3pagetiff2.pdf");
            //pdfWriter.convertTiff3(url + "3pagetiff.tif", url + "3pagetiff3.pdf");
            pdfWriter.convertTiff4(url + "3pagetiff.tif", url + "3pagetiff4.pdf");

            //pdfWriter.convertTiff1(url + "AASF.tif", url + "AASF1.pdf");
            //pdfWriter.convertTiff2(url + "AASF.tif", url + "AASF2.pdf");
            //pdfWriter.convertTiff3(url + "AASF.tif", url + "AASF3.pdf");
            pdfWriter.convertTiff4(url + "AASF.tif", url + "AASF4.pdf");

            pdfWriter.convertTiff4(url + "JpegTechnote2_MinIsBlack_Page2.TIF", url + "JpegTechnote2_MinIsBlack_Page24.pdf");
            pdfWriter.convertTiff4(url + "JpegOrigTiff60_YCbCr.tif", url + "JpegOrigTiff60_YCbCr4.pdf");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

