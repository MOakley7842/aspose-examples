package com.examples.aspose;

import com.aspose.words.Document;
import com.examples.aspose.AsposeLicense;


/**
 * Created by moakley on 2/24/15.
 */
public class WordToPdfConvert {


    private void convertWord(String filename, String filesave) {
        try {
            System.out.println("os.name=" + System.getProperty("os.name"));
            System.out.println("os.version=" + System.getProperty("os.version"));
            System.out.println("java.version=" + System.getProperty("java.version"));

            Document document = new Document(filename);

            System.out.println("filename: " + filename + " - " + document.getPageCount());

            for (int i = 0; i < document.getPageCount(); i++) {
                System.out.println("Height: " + document.getPageInfo(i).getHeightInPoints());
                System.out.println("Width : " + document.getPageInfo(i).getWidthInPoints());
                //System.out.println(" : " + document.getPageInfo().);
            }
            com.aspose.words.PdfSaveOptions pdfSaveOptions = new com.aspose.words.PdfSaveOptions();
            pdfSaveOptions.setPageIndex(1);
            pdfSaveOptions.setPageCount(18);
            document.save(filesave, pdfSaveOptions);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {
        try {
            //AsposeLicense.setLicenseByType(AsposeLicense.ASPOSE_WORDS);

            WordToPdfConvert wordToPdf = new WordToPdfConvert();

            String url = WordToPdfConvert.class.getResource("/test/image/").getPath();

            wordToPdf.convertWord(url + "ICMArchitectureOverview.docx", url + "ICMArchitectureOverview.pdf");

            //wordToPdf.convertWord(url + "Template.doc", url + "Template.pdf");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
