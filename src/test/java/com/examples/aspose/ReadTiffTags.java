package com.examples.aspose;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.*;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

import com.sun.media.imageio.plugins.tiff.TIFFDirectory;
import com.sun.media.imageio.plugins.tiff.TIFFField;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;

import java.awt.image.RenderedImage;
import java.awt.image.renderable.ParameterBlock;
import javax.imageio.stream.ImageOutputStream;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;
//import com.sun.media.jai.codec.FileSeekableStream;
//import com.sun.media.jai.codec.ImageCodec;
//import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
//import java.io.File;
//import java.io.IOException;

/**
 * Created by moakley on 4/22/15.
 */
public class ReadTiffTags {

    public void readTiffImageProperties(String inputTifImagePath) {
        Iterator readersIterator = ImageIO.getImageReadersByFormatName("tif");
        ImageReader imageReader = (ImageReader)readersIterator.next();
        ImageInputStream imageInputStream;
        try {
            System.out.println("Filename:  " + inputTifImagePath);
            imageInputStream = new FileImageInputStream(new File(inputTifImagePath));
            imageReader.setInput(imageInputStream,false, true);

/* Take a input from a file */
            FileSeekableStream fileSeekableStream;
            fileSeekableStream = new FileSeekableStream(inputTifImagePath);

/* create ImageDecoder to count your pages from multi-page tiff */
            ImageDecoder iDecoder = ImageCodec.createImageDecoder("tiff", fileSeekableStream, null);

/* count the number of pages inside the multi-page tiff */
            int pageCount = iDecoder.getNumPages();

/* use first for loop to get pages one by one */
            for(int page = 0; page < pageCount; page++){
/* get image metadata for each page */
                IIOMetadata imageMetadata = imageReader.getImageMetadata(page);

/*
 * The root of all the tags for this image is the IFD (Image File Directory).
 * Get the IFD from where we can get all the tags for the image.
 */
                TIFFDirectory ifd = TIFFDirectory.createFromMetadata(imageMetadata);

/* Create a Array of TIFFField*/
                TIFFField[] allTiffFields = ifd.getTIFFFields();

/* use second for loop to get all field data */
                for (int i = 0; i < allTiffFields.length; i++) {
                    TIFFField tiffField = allTiffFields[i];

/* name of property */
                    String nameOfField = tiffField.getTag().getName();

/* Tag no. of the property (optional) */
                    int numberOfField = tiffField.getTagNumber();

/* Type of property (optional) */
                    String typeOfField = TIFFField.getTypeName(tiffField.getType());

/* Value of Property*/
                    String valueOfField = tiffField.getValueAsString(0);

/* print it down as per your way */
                    System.out.println((i+1)+". " + nameOfField + ", " + numberOfField + ", " + typeOfField + ", " + valueOfField);
                }
/* just for separate between two page (image) property */
                System.out.println("======================================");
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static void extractMultiPageTiff(String tiffFilePath,
                                            String outputFileType) throws IOException {

     /*
     * create object of RenderedIamge to produce
     * image data in form of Rasters
     */
        RenderedImage renderedImage[], page;
        File file = new File(tiffFilePath);

        System.out.println("Filename:  " + tiffFilePath);
     /*
     * SeekabaleStream is use for taking input from file.
     * FileSeekableStream is not committed part of JAI API.
     */
        SeekableStream seekableStream = new FileSeekableStream(file);
        ImageDecoder imageDecoder = ImageCodec.createImageDecoder("tiff",
                seekableStream, null);
        renderedImage = new RenderedImage[imageDecoder.getNumPages()];

     /* count no. of pages available inside input tiff file */
        int count = 0;
        for (int i = 0; i < imageDecoder.getNumPages(); i++) {
            renderedImage[i] = imageDecoder.decodeAsRenderedImage(i);
            count++;
        }

     /* set output folder path */
        String outputFolderName;
        String[] temp = null;
        temp = tiffFilePath.split("\\.");
        outputFolderName = temp[0];
     /*
     * create file object of output folder
     * and make a directory
     */
        File fileObjForOPFolder = new File(outputFolderName);
        fileObjForOPFolder.mkdirs();

     /*
     * extract no. of image available inside
     * the input tiff file
     */
        for (int i = 0; i < count; i++) {
            page = imageDecoder.decodeAsRenderedImage(i);
            File fileObj = new File(outputFolderName
                    + "/" + (i + 1) + ".jpg");
         /*
         * ParameterBlock create a generic
         * interface for parameter passing
         */
            ParameterBlock parameterBlock = new ParameterBlock();
         /* add source of page */
            parameterBlock.addSource(page);
         /* add o/p file path */
            parameterBlock.add(fileObj.toString());
         /* add o/p file type */
            parameterBlock.add(outputFileType);
         /* create output image using JAI filestore */
            RenderedOp renderedOp = JAI.create("filestore",
                    parameterBlock);
            renderedOp.dispose();
        }
    }


    private void writeJpegImage(ImageWriter imageWriter, RenderedImage image, File fileObj) throws IOException{

        ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(fileObj);
        try
        {
            ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();
            imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            imageWriteParam.setCompressionQuality(0.3f);
            imageWriter.setOutput(imageOutputStream);
            imageWriter.write(null, new IIOImage(image, null, null), imageWriteParam);
        }
        catch(IOException ex){
            ex.printStackTrace();
            throw ex;
        }
        finally{
            if (imageOutputStream != null) {
                imageOutputStream.flush();
                imageOutputStream.close();
            }
        }
    }


    public static void main(String[] args) throws Exception {
        try {
            ReadTiffTags tiffTags = new ReadTiffTags();

            String url = TiffToPdfConvert.class.getResource("/test/image/").getPath();

            tiffTags.readTiffImageProperties(url + "3pagetiff.tif");
            //tiffTags.readTiffImageProperties(url + "AASF.tif");

            //tiffTags.extractMultiPageTiff(url + "3pagetiff.tif", "JPEG");
            //tiffTags.extractMultiPageTiff(url + "AASF.tif","JPEG");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
