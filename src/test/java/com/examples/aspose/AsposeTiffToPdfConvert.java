package com.examples.aspose;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import aspose.pdf.Image;
import com.aspose.imaging.Color;
import com.aspose.imaging.LoadOptions;
import com.aspose.imaging.fileformats.jpeg.JpegCompressionColorMode;
import com.aspose.imaging.fileformats.jpeg.JpegCompressionMode;
import com.aspose.imaging.fileformats.jpeg.JpegImage;
import com.aspose.imaging.fileformats.png.PngColorType;
import com.aspose.imaging.fileformats.png.PngImage;
import com.aspose.imaging.fileformats.tiff.TiffFrame;
import com.aspose.imaging.fileformats.tiff.TiffImage;
import com.aspose.imaging.imageoptions.JpegOptions;
import com.aspose.imaging.imageoptions.PngOptions;
import com.aspose.imaging.sources.FileCreateSource;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

/**
 * Created by moakley on 5/4/15.
 */
public class AsposeTiffToPdfConvert {



    private void convertTiff(String filename, String myDir) {
        try {
            TiffImage multiImage = (TiffImage) com.aspose.imaging.Image.load(filename);
            ArrayList<String> imageFiles = new ArrayList<>();
            String imageFile;

            int pageNumber = 1;
            for (TiffFrame tiffFrame : multiImage.getFrames())
            {
                multiImage.setActiveFrame(tiffFrame);
                int bitsPerPixel = multiImage.getBitsPerPixel();


                //Color[] pixels = multiImage.loadPixels(tiffFrame.getBounds());


                if (bitsPerPixel == 1) {
                    PngOptions pngOptions = new PngOptions();
                    pngOptions.setColorType(PngColorType.Grayscale);

                    imageFile = String.format(myDir + "Concat" + pageNumber + ".png");
                    pngOptions.setSource(new FileCreateSource(imageFile, false));
                    //PngImage pngImage = (PngImage) com.aspose.imaging.Image.create(pngOptions,
                    //        tiffFrame.getWidth(), tiffFrame.getHeight());
                    //int bits = pngImage.getBitsPerPixel();
                    System.out.println("filename: " + filename + " - " + pageNumber + " - " + bitsPerPixel);

                    //pngImage.savePixels(tiffFrame.getBounds(), pixels);
                    //pngImage.save();
                    multiImage.save(imageFile, pngOptions);
                }
                else {
                    JpegOptions jpegOptions = new JpegOptions();

                    jpegOptions.setQuality(30);

                    // Did not make a difference
                    //if (bitsPerPixel == 8)
                    //    jpegOptions.setColorType(JpegCompressionColorMode.Grayscale);

                    imageFile = String.format(myDir + "Concat" + pageNumber + ".jpg");
                    jpegOptions.setSource(new FileCreateSource(imageFile, false));
                    //JpegImage jpgImage = (JpegImage) com.aspose.imaging.Image.create(jpegOptions,
                    //        tiffFrame.getWidth(), tiffFrame.getHeight());
                    //int bits = jpgImage.getBitsPerPixel();
                    System.out.println("filename: " + filename + " - " + pageNumber + " - " + bitsPerPixel);

                    //jpgImage.savePixels(tiffFrame.getBounds(), pixels);
                    //jpgImage.save();
                    multiImage.save(imageFile, jpegOptions);
                }

                imageFiles.add(imageFile);

                pageNumber++;
            }


            PDDocument pdfDocument = new PDDocument();
            PDPage pdPage;
            PDXObjectImage pdxImage;

            for (String imageFilename : imageFiles) {

                if (imageFilename.endsWith("jpg")) {
                    pdxImage = new PDJpeg(pdfDocument,
                            new FileInputStream(imageFilename));

                }
                else {
                    BufferedImage awtImage = ImageIO.read( new File( imageFilename ) );
                    pdxImage = new PDPixelMap(pdfDocument, awtImage);
                }

                pdPage = new PDPage(
                        new PDRectangle(pdxImage.getWidth(), pdxImage.getHeight()));

                pdfDocument.addPage(pdPage);
                PDPageContentStream contentStream = new PDPageContentStream(pdfDocument, pdPage, true, true);
                contentStream.drawImage(pdxImage, 0, 0);
                contentStream.close();

            }

            String pdfFile = String.format(myDir + ".pdf");
            pdfDocument.save(pdfFile);
            pdfDocument.close();

        }
        catch (Exception de) {
            de.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {
        try {
            //AsposeLicense.setLicenseByType(AsposeLicense.ASPOSE_PDF);

            AsposeTiffToPdfConvert tiffToPdf = new AsposeTiffToPdfConvert();

            String url = TiffToPdfConvert.class.getResource("/test/image/").getPath();


            tiffToPdf.convertTiff(url + "3pagetiff.tif", url + "3pagetiff");
            //tiffToPdf.convertTiff(url + "AASF.tif", url + "AASF");
            //tiffToPdf.convertTiff(url + "JpegOrigTiff60_YCbCr.tif", url + "JpegOrigTiff60_YCbCr");
            //tiffToPdf.convertTiff(url + "JpegTechnote2_MinIsBlack_Page2.TIF", url + "JpegTechnote2_MinIsBlack_Page2");
            //tiffToPdf.convertTiff(url + "LZW_Pallette_1Bit.tif", url + "LZW_Pallette_1Bit");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
