package com.examples.aspose;

import java.io.*;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import aspose.pdf.Image;
import aspose.pdf.MarginInfo;
import aspose.pdf.Pdf;
import aspose.pdf.Section;
import com.aspose.imaging.Color;
import com.aspose.imaging.LoadOptions;
import com.aspose.imaging.fileformats.tiff.TiffImage;
import com.aspose.imaging.sources.FileCreateSource;
import com.aspose.pdf.Page;
import com.aspose.pdf.generator.legacyxmlmodel.ImageFileType;
import com.aspose.words.Document;
import com.aspose.imaging.Image.*;

import javax.imageio.*;
import java.util.Iterator;
/**
 * Created by Mike.Oakley on 2/24/2015.
 */
public class TiffToPdfConvert {

    private void convertTiff1(String filename, String filesave) {
        try {
// read the source TIFF image
            ImageInputStream iis = ImageIO.createImageInputStream(new FileInputStream(filename));
            String [] readers = ImageIO.getReaderFormatNames();
            for (int i = 0; i < readers.length; i++) {
                System.out.println(readers[i].toString());
            }

            // need to load addition image readers for Tiff.  JAI ImageIO likely specific by environment.
            Iterator<ImageReader> iReaders = ImageIO.getImageReaders(iis);
            if (!iReaders.hasNext()){
                throw new IllegalStateException("no jpeg image writers found");
            }

            ImageReader ir = iReaders.next();
            ir.setInput(iis);
            int frameCount = ir.getNumImages(true);
            for (int i = 0; i < frameCount; ++i)
                ImageIO.write(ir.read(i), "jpg", new File("D:\\Temp\\Linux\\Pdf\\images\\frame" + i + ".jpg"));


            String image_Directory = "D:\\Temp\\Linux\\Pdf\\images\\";
            File f = new File(image_Directory);
            File[] fa = f.listFiles();

            Pdf pdf1 = new Pdf();
            Section sec1 = pdf1.getSections().add();

            for (int i = 0; i < fa.length; i++)
            {
                aspose.pdf.Image img1 = new aspose.pdf.Image(sec1);
                sec1.getParagraphs().add(img1);
                img1.getImageInfo().setFile(fa[i].getPath());
                img1.getImageInfo().setImageFileType(ImageFileType.Jpeg);
            }
            pdf1.save(filesave);
            iis.close();
        }
        catch (Exception de) {
            System.out.println("Exception: " + de.getMessage());
        }
    }

    private void convertTiff2(String filename, String filesave) {
        try {
            Pdf pdf1 = new Pdf();
            Section sec1 = pdf1.getSections().add();
            aspose.pdf.Image image = new aspose.pdf.Image(sec1);
            //sec1.getParagraphs().add(image);
            //ImageInputStream iis = ImageIO.createImageInputStream(new FileInputStream(filename));
            //image.getImageInfo().setSystemImage(ImageIO.read(iis));
            //image.getImageInfo().setImageFileType(ImageFileType.Tiff);
            //image.getImageInfo().setTiffFrame(1);
            //pdf1.save(filesave);
            //iis.close();
            System.out.println("Page count: " + sec1.getPageCount());
            image.getImageInfo().setFile(filename);
            image.getImageInfo().setTiffFrame(-1);
            image.getImageInfo().setImageFileType(aspose.pdf.ImageFileType.Tiff);
            //for (int i = 0; i < sec1.getPageCount(); i++) {
            sec1.getParagraphs().add(image);
            //}

            pdf1.save(filesave);
        }
        catch (Exception de) {
            System.out.println("Exception: " + de.getMessage());
        }

    }

    private void convertTiff3(String filename, String filesave) {
        try {
            //Document document =  new Document();
            Pdf pdf = new Pdf();
            Section section = pdf.getSections().add();
            Image image = new Image(section);

            image.getImageInfo().setImageFileType(ImageFileType.Tiff);
            image.getImageInfo().setFile(filename);
            image.getImageInfo().isAllFramesInNewPage(true);
            image.getImageInfo().setTiffFrame(-1);

            //image.setImageScale(.5f);  // just makes the page smaller.  top-left justified

            //System.out.println("BitsPerComponent: " + image.getImageInfo().getBitsPerComponent());  0 - nothing loaded yet

            section.getParagraphs().add(image);
            MarginInfo marginInfo = new MarginInfo();
            marginInfo.setBottom(0f);
            marginInfo.setLeft(0f);
            marginInfo.setRight(0f);
            marginInfo.setTop(0f);
            section.getPageInfo().setMargin(marginInfo);

            pdf.save(filesave);
        }
        catch (Exception de) {
            System.out.println("Exception: " + de.getMessage());
        }
    }

    private void convertTiff4(String filename, String filesave)
    {
        try {
            com.aspose.pdf.Document doc = new com.aspose.pdf.Document();

            doc.getPages().add();
            com.aspose.pdf.Image img = new com.aspose.pdf.Image();

            img.setFile(filename);

            doc.getPages().get_Item(1).getParagraphs().add(img);
            com.aspose.pdf.MarginInfo marginInfo = new com.aspose.pdf.MarginInfo();
            marginInfo.setBottom(0f);
            marginInfo.setLeft(0f);
            marginInfo.setRight(0f);
            marginInfo.setTop(0f);
            doc.getPageInfo().setMargin(marginInfo);
            doc.save(filesave);
        }
        catch (Exception de) {
            System.out.println("Exception: " + de.getMessage());
        }
    }

    private void convertTiff5(String filename, String filesave) {
        try {
            // instantiate Document object

            com.aspose.pdf.Document doc = new com.aspose.pdf.Document();

// add page to PDF file
            doc.getPages().add();

// create Image object
            com.aspose.pdf.Image img = new com.aspose.pdf.Image();

// load TIFF image
            img.setFile(filename);

// add image to paragraphs collection of first page
            doc.getPages().get_Item(1).getParagraphs().add(img);


            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            com.aspose.pdf.MarginInfo marginInfo = new com.aspose.pdf.MarginInfo();
            marginInfo.setBottom(0f);
            marginInfo.setLeft(0f);
            marginInfo.setRight(0f);
            marginInfo.setTop(0f);
            doc.getPageInfo().setMargin(marginInfo);

// save resultant PDF
            doc.save(outputStream);

            doc= new com.aspose.pdf.Document(new ByteArrayInputStream(outputStream.toByteArray()));

            com.aspose.pdf.Document.OptimizationOptions opt = new com.aspose.pdf.Document.OptimizationOptions();
            opt.setRemoveUnusedObjects ( false );
            opt.setLinkDuplcateStreams ( false );
            opt.setRemoveUnusedStreams ( false );

// Enable image compression
            opt.setCompressImages ( true );

// Set the quality of images in PDF file
            opt.setImageQuality (25);
            doc.optimizeResources(opt);

            doc.save(filesave);
        }
        catch (Exception de) {
            System.out.println("Exception: " + de.getMessage());
        }
    }


    public static void main(String[] args) throws Exception {
        try {
            //AsposeLicense.setLicenseByType(AsposeLicense.ASPOSE_PDF);

            TiffToPdfConvert tiffToPdf = new TiffToPdfConvert();

            String url = TiffToPdfConvert.class.getResource("/test/image/").getPath();

            //tiffToPdf.convertTiff1(url + "3pagetiff.tif", url + "3pagetiff1.pdf");
            //tiffToPdf.convertTiff1(url + "AASF.tif", url + "AASF1.pdf");

            //tiffToPdf.convertTiff2(url + "3pagetiff.tif", url + "3pagetiff2.pdf");
            //tiffToPdf.convertTiff2(url + "AASF.tif", url + "AASF2.pdf");

            //tiffToPdf.convertTiff3(url + "3pagetiff.tif", url + "3pagetiff3.pdf");
            //tiffToPdf.convertTiff3(url + "AASF.tif", url + "AASF3.pdf");

            //tiffToPdf.convertTiff4(url + "3pagetiff.tif", url + "3pagetiff4.pdf");
            //tiffToPdf.convertTiff4(url + "AASF.tif", url + "AASF4.pdf");

            //tiffToPdf.convertTiff5(url + "3pagetiff.tif", url + "3pagetiff5.pdf");
            //tiffToPdf.convertTiff5(url + "AASF.tif", url + "AASF5.pdf");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
