package com.examples.aspose;

import com.examples.aspose.AsposeLicense;
import com.aspose.slides.*;

import java.awt.image.BufferedImage;
import java.net.URL;

/**
 * Created by moakley on 2/24/15.
 */
public class PptToPdfConvert {

    private void convertSlides(String filename, String filesave) {
        try {
            System.out.println("os.name=" + System.getProperty("os.name"));
            System.out.println("os.version=" + System.getProperty("os.version"));
            System.out.println("java.version=" + System.getProperty("java.version"));

            Presentation pres = new Presentation(filename);
            System.out.println("filename = " + filename + " - " + pres.getSlides().size());
            PdfOptions pdfOptions = new PdfOptions();
            pdfOptions.setJpegQuality((byte)96);

            /*
            int slideCount = pres.getSlides().size();
            for (int i = 0; i < slideCount; i++) {
                Slide slide = (Slide)pres.getSlides().get_Item(i);
                BufferedImage bufferedImage = slide.getThumbnail();
            }
            */

            pres.save(filesave, SaveFormat.Pdf, pdfOptions);

        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        try {
            //AsposeLicense.setLicenseByType(AsposeLicense.ASPOSE_SLIDES);

            PptToPdfConvert pptToPdf = new PptToPdfConvert();

            String url = PptToPdfConvert.class.getResource("/test/image/").getPath();

            pptToPdf.convertSlides(url + "2014Copy Page 16.pptx", url + "2014Copy Page 16.pdf");
            pptToPdf.convertSlides(url + "2014Copy Page 20.pptx", url + "2014Copy Page 20.pdf");
            //pptToPdf.convertSlides(url + "PrefillPPT.pptx", url + "PrefillPPT.pdf");

            //pptToPdf.convertSlides(url + "SharePoint2011.pptx", url + "SharePoint2011.pdf");

            //pptToPdf.convertSlides(url + "SQL_ODBC.ppt", url + "SQL_ODBC.pdf");

            //pptToPdf.convertSlides(url + "2014Copy (25) - Page 9.pptx", url + "2014Copy (25) - Page 9.pdf");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
