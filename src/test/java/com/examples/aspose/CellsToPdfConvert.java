package com.examples.aspose;

import java.io.*;

import com.aspose.cells.*;
import com.examples.aspose.AsposeLicense;

import com.aspose.pdf.SaveFormat;

import javax.imageio.*;
import java.util.Iterator;

/**
 * Created by moakley on 2/24/15.
 */
public class CellsToPdfConvert {


    private void convertCells1(String filename, String filesave) {
        try {
            System.out.println(com.aspose.cells.CellsHelper.getVersion());
            Workbook workbook = new Workbook(new FileInputStream(filename));

            System.out.println("filename: " + filename + " - " + workbook.getWorksheets().getCount());

            //com.aspose.cells.Worksheet worksheet = workbook.getWorksheets().get(0);
            //Cells cells = worksheet.getCells();
            //System.out.println("getHorizontalAlignment A2: " + cells.get("A2").getStyle().getHorizontalAlignment());
            //System.out.println("getHorizontalAlignment B2: " + cells.get("B2").getStyle().getHorizontalAlignment());
            //System.out.println("getHorizontalAlignment C2: " + cells.get("C2").getStyle().getHorizontalAlignment());
            //System.out.println("getHorizontalAlignment D2: " + cells.get("D2").getStyle().getHorizontalAlignment());
            //System.out.println("getHorizontalAlignment E2: " + cells.get("E2").getStyle().getHorizontalAlignment());
            //System.out.println("getHorizontalAlignment F2: " + cells.get("F2").getStyle().getHorizontalAlignment());
            //System.out.println("getHorizontalAlignment G2: " + cells.get("G2").getStyle().getHorizontalAlignment());

            //com.aspose.cells.Cell cell = cells.get("A1");
            //Style style = cell.getStyle();
            //style.setHorizontalAlignment(TextAlignmentType.CENTER);
            //cell.setStyle(style);
            //style = cell.getStyle();
            //System.out.println("getHorizontalAlignment A1: " + cell.getStyle().getHorizontalAlignment());

            com.aspose.cells.PdfSaveOptions options = new com.aspose.cells.PdfSaveOptions();

            options.setCreatedTime(DateTime.getNow());
            //options.setAllColumnsInOnePagePerSheet(true);
            options.setPageCount(3);
            options.setPageIndex(0);
            //options.setOnePagePerSheet(true);
            //options.setCalculateFormula(false);

            //WorksheetCollection wsColl = workbook.getWorksheets();

            workbook.save(filesave, options);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void convertCells2(String filename, String filesave) {
        try {
            System.out.println(com.aspose.cells.CellsHelper.getVersion());
            Workbook workbook = new Workbook(new FileInputStream(filename));

            System.out.println("filename: " + filename + " - " + workbook.getWorksheets().getCount());

            for (int i = 0 ; i < workbook.getWorksheets().getCount() ; i++)
            {
                Worksheet worksheet = workbook.getWorksheets().get(i);
                //worksheet.getPageSetup().setBottomMargin(0d);
                //worksheet.getPageSetup().setLeftMargin(0d);
                worksheet.getPageSetup().setRightMargin(0d);
                //worksheet.getPageSetup().setTopMargin(0d);
                //worksheet.getPrintingPageBreaks()
                //if(worksheet.getIndex() == 0)
                //{
                //worksheet.getPageSetup().setPrintArea("");
                worksheet.getPageSetup().setOrientation(PageOrientationType.LANDSCAPE);
                worksheet.getPageSetup().setPaperSize(PaperSizeType.PAPER_LETTER);

                ImageOrPrintOptions opts = new ImageOrPrintOptions();
                SheetRender sr = new SheetRender(worksheet, opts);
                System.out.println("SheetRender 1: " + i + "  " + sr.getPageCount());

                opts.setAllColumnsInOnePagePerSheet(true);
                sr = new SheetRender(worksheet, opts);
                System.out.println("SheetRender 2: " + i + "  " + sr.getPageCount());
                //}
                //else
                //{
                //    worksheet.setVisible(false);
                //}
            }

            workbook.save(filesave, SaveFormat.Pdf);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void convertCells3(String filename, String filesave) {
        try {
            System.out.println(com.aspose.cells.CellsHelper.getVersion());
            Workbook workbook = new Workbook(new FileInputStream(filename));

            System.out.println("filename: " + filename + " - " + workbook.getWorksheets().getCount());

            com.aspose.cells.PdfSaveOptions options = new com.aspose.cells.PdfSaveOptions();

            options.setCreatedTime(DateTime.getNow());
            //options.setAllColumnsInOnePagePerSheet(true);
            //options.setPageCount(1);
            //options.setPageIndex(0);
            options.setOnePagePerSheet(true);
            //options.setCalculateFormula(false);

            WorksheetCollection wsColl = workbook.getWorksheets();

            workbook.save(filesave, options);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void main(String[] args) throws Exception {
        try {
            AsposeLicense.setLicenseByType(AsposeLicense.ASPOSE_CELLS);

            CellsToPdfConvert cellsToPdf = new CellsToPdfConvert();

            String url = CellsToPdfConvert.class.getResource("/test/image/").getPath();

            cellsToPdf.convertCells1(url + "SampleDataNew.xlsx", url + "SampleDataNew1.pdf");
            cellsToPdf.convertCells1(url + "ColorSheets2.xlsx", url + "ColorSheets21.pdf");
            cellsToPdf.convertCells1(url + "ClaimSomeColor.xls", url + "ClaimSomeColor1.pdf");

            cellsToPdf.convertCells2(url + "SampleDataNew.xlsx", url + "SampleDataNew2.pdf");
            cellsToPdf.convertCells2(url + "ColorSheets2.xlsx", url + "ColorSheets22.pdf");
            cellsToPdf.convertCells2(url + "ClaimSomeColor.xls", url + "ClaimSomeColor2.pdf");

            cellsToPdf.convertCells3(url + "SampleDataNew.xlsx", url + "SampleDataNew3.pdf");
            cellsToPdf.convertCells3(url + "ColorSheets2.xlsx", url + "ColorSheets23.pdf");
            cellsToPdf.convertCells3(url + "ClaimSomeColor.xls", url + "ClaimSomeColor3.pdf");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
